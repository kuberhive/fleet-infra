# Fleet Infrastructure Suite

Welcome to our Fleet Infrastructure Suite, dedicated to cluster bootstrapping and providing a robust platform characterized by [GitOps](https://opengitops.dev/about) principles, the [ArgoCD app of apps pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/#app-of-apps-pattern), and [Kubernetes native](https://landscape.cncf.io/) architecture.

## Key Features

- **GitOps Driven:** Embrace the GitOps methodology for declarative configuration management and continuous delivery of infrastructure changes.
- **ArgoCD App of Apps Pattern:** Leverage the power of ArgoCD's app of apps pattern for efficient and scalable application deployment and management.
- **Kubernetes Native:** Harness the full potential of Kubernetes, ensuring seamless integration and optimal performance within your infrastructure. Referencing the [CNCF Trailmap](https://landscape.cncf.io/), our suite aligns with industry best practices and standards for cloud-native infrastructure.

## Application Groups

Our suite encompasses various application groups, each designed to fulfill specific roles and functionalities within the infrastructure ecosystem:

| Name | Status | Description |
| --- | --- | --- |
| [main](.) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=main&revision=true&showAppName=true) | Core components and configurations |
| [infra-mgr](./docs/infra-mgr.md) | ![infra-mgr](https://cd.jcan.dev/api/badge?name=infra-mgr&revision=true&showAppName=true) | Infrastructure management applications |
| [infra-ops](./docs/infra-ops.md) | ![infra-ops](https://cd.jcan.dev/api/badge?name=infra-ops&revision=true&showAppName=true) | Applications for streamlining cluster operations |
| [media-centre](./docs/media-centre.md) | ![media-centre](https://cd.jcan.dev/api/badge?name=media-centre&revision=true&showAppName=true) | Media center applications for content management |

Our suite is meticulously crafted to provide a seamless and efficient infrastructure management experience, ensuring scalability, reliability, and ease of use.

Ready to streamline your infrastructure operations? Dive into our suite of applications and transform your cluster management experience.
