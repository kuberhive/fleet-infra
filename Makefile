.PHONY: cluster-install cluster-clean
CLUSTER=multi-node
cluster-install:
	kind create cluster --config=$(CLUSTER).yaml --name=$(CLUSTER)
cluster-clean:
	kind delete cluster --name=$(CLUSTER)

.PHONY: argocd-install argocd-proxy argocd-password argocd-login argocd-clean argocd-wait
NAMESPACE=argo-cd
ARGOCD_PASSWORD=$$(kubectl -n $(NAMESPACE) get secret argocd-initial-admin-secret -o yaml | grep -o 'password: .*' | sed -e s"/password\: //g" | base64 -d)
argocd-install:
	# kubectl create ns $(NAMESPACE)
	helm -n $(NAMESPACE) install argo-cd argo/argo-cd --values=./src/projects/values/argo-cd.yaml
	$(MAKE) argocd-wait

argocd-wait:
	kubectl -n $(NAMESPACE) wait -l app.kubernetes.io/name=argo-cd-server --for=condition=ready pod --timeout=360s

argocd-proxy:
	kubectl -n $(NAMESPACE) port-forward svc/argo-cd-server 8080:80

argocd-password:
	@echo "ARGOCD_PASSWORD: $(ARGOCD_PASSWORD)"

argocd-login: argocd-password
	@argocd login localhost:8080 --insecure --username="admin" --password="$(ARGO_PASSWORD)"

argocd-bootstrap: argocd-login
	argocd app create main \
		--dest-namespace $(NAMESPACE) \
		--dest-server https://kubernetes.default.svc \
		--repo https://gitlab.com/kuberhive/fleet-infra.git \
		--path src/main  
	argocd app sync main

.PHONY: bootstrap
bootstrap: bootstrap-cluster bootstrap-argocd

# Internal Services
# LONGHORN
.PHONY: longhorn-proxy
longhorn-proxy:
	kubectl -n longhorn port-forward svc/longhorn-frontend 8081:80

# ARGO WORKFLOWS
.PHONY: workflow-token
ARGO_TOKEN="Bearer $$(kubectl -n argo-workflows get secret server.service-account-token -o=jsonpath='{.data.token}' | base64 --decode)"
workflow-token:
	@echo $(ARGO_TOKEN)

# MONITORING STACK
.PHONY: grafana-password grafana-proxy
GRAFANA_NAMESPACE=kube-prometheus-stack
GRAFANA_PASSWORD=$$(kubectl -n $(GRAFANA_NAMESPACE) get secret $(GRAFANA_NAMESPACE)-grafana -o=jsonpath='{.data.admin-password}' | base64 -d)
grafana-password:
	@echo $(GRAFANA_PASSWORD)
grafana-proxy:
	kubectl -n $(GRAFANA_NAMESPACE) port-forward svc/$(GRAFANA_NAMESPACE)-grafana 8080:80

OPENBAO_NAMESPACE=openbao
openbao-shell:
	kubectl -n $(OPENBAO_NAMESPACE) exec -ti pod/openbao-0 -- sh

openbao-init:
	kubectl -n $(OPENBAO_NAMESPACE) exec -ti pod/openbao-0 -- sh -c 'bao operator init -format=json'

openbao-unseal:
	kubectl -n $(OPENBAO_NAMESPACE) exec -ti pod/openbao-0 -- sh -c 'bao operator unseal'