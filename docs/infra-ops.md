# Infrastructure Operations Suite

Welcome to our Infrastructure Operations Suite, where efficiency meets automation in managing your cluster operations. Our collection of applications, meticulously crafted with chart and kustomize manifests, ensures seamless cluster management, keeping your tools operational and enabling various users to perform their assigned tasks effortlessly.

## Featured Applications

Explore our featured applications below, each designed to streamline and optimize specific aspects of cluster operations:

| Name | Status | Description |
| --- | --- | --- |
| [k3s-upgrader](../src/infra-ops/kustomize/k3s-upgrader/kustomization.yaml) | ![infra-ops](https://cd.jcan.dev/api/badge?name=k3s-upgrader&revision=true&showAppName=true) | Framework for upgrading k3s clusters |
| [k3s-upgrader-plans](../src/infra-ops/kustomize/k3s-upgrader-plans/kustomization.yaml) | ![infra-ops](https://cd.jcan.dev/api/badge?name=k3s-upgrader-plans&revision=true&showAppName=true) | Plans for upgrading k3s clusters |
| [letsencrypt](../src/infra-ops/kustomize/letsencrypt/kustomization.yaml) | ![infra-ops](https://cd.jcan.dev/api/badge?name=letsencrypt&revision=true&showAppName=true) | Provisioner for Let's Encrypt certificates |
| [secrets-integrator](../src/infra-ops/kustomize/secrets-integrator/kustomization.yaml) | ![infra-ops](https://cd.jcan.dev/api/badge?name=secrets-integrator&revision=true&showAppName=true) | Application for integrating credentials securely |
| [webhooks](../src/infra-ops/kustomize/webhooks/kustomization.yaml) | ![infra-ops](https://cd.jcan.dev/api/badge?name=webhooks&revision=true&showAppName=true) | Enabler for workflow webhooks |
| [workflow-templates](../src/infra-ops/kustomize/workflow-templates/kustomization.yaml) | ![infra-ops](https://cd.jcan.dev/api/badge?name=workflow-templates&revision=true&showAppName=true) | Templates for workflow management |

Our suite empowers you to manage your cluster operations efficiently, ensuring smooth upgrades, secure credential integration, and streamlined workflows.

Ready to optimize your cluster operations? Dive into our suite of applications and elevate your infrastructure management experience.
