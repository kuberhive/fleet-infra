# Infrastructure Management Suite

Welcome to our comprehensive Infrastructure Management Suite, designed to streamline and optimize your infrastructure operations. Our curated selection of applications is meticulously chosen based on the latest recommendations from the Cloud Native Computing Foundation ([CNCF](https://raw.githubusercontent.com/cncf/trailmap/master/CNCF_TrailMap_latest.png)), ensuring that you have access to cutting-edge tools for managing your infrastructure effectively.

## Featured Applications

Explore our featured applications below, each handpicked to address specific aspects of infrastructure management:

| Application | Status | Description |
| --- | --- | --- |
| [Argo CD](../src/infra-mgr/values/argo-cd.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=argo-cd&revision=true&showAppName=true) | Continuous Delivery with GitOps practices |
| [Argo Events](../src/infra-mgr/values/argo-events.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=argo-events&revision=true&showAppName=true) | Event-driven automation framework |
| [Argo Workflows](../src/infra-mgr/values/argo-workflows.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=argo-workflows&revision=true&showAppName=true) | Workflow designer for complex automation tasks |
| [Cert Manager](../src/infra-mgr/values/cert-manager.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=cert-manager&revision=true&showAppName=true) | Secure management of TLS certificates |
| [External Secrets](../src/infra-mgr/values/external-secrets.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=external-secrets&revision=true&showAppName=true) | Integration framework for managing secrets securely |
| [GitLab Runner](../src/infra-mgr/values/gitlab-runner.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=gitlab-runner&revision=true&showAppName=true) | CI/CD execution agent for GitLab pipelines |
| [Harbor](../src/infra-mgr/values/harbor.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=harbor&revision=true&showAppName=true) | Helm chart repository and container image registry |
| [Kafka](../src/infra-mgr/values/kafka.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=kafka&revision=true&showAppName=true) | Open-source distributed event streaming platform |
| [Longhorn](../src/infra-mgr/values/longhorn.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=longhorn&revision=true&showAppName=true) | Distributed block storage for Kubernetes |
| [NFS Provisioner](../src/infra-mgr/values/nfs-provisioner.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=nfs-provisioner&revision=true&showAppName=true) | Provisioning external NFS storage dynamically |
| [NVIDIA Operator](../src/infra-mgr/values/nvidia-operator.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=nvidia-operator&revision=true&showAppName=true) | Kubernetes operator for NVIDIA GPU management |
| [Vault](../src/infra-mgr/values/vault.yaml) | ![fleet-infra](https://cd.jcan.dev/api/badge?name=vault&revision=true&showAppName=true) | Securely manage secrets and protect sensitive data |

Our suite empowers you to orchestrate, automate, and secure your infrastructure with ease, ensuring scalability, reliability, and efficiency in your operations.

Ready to take control of your infrastructure? Dive into our suite of applications and unlock the full potential of your cloud-native environment.
