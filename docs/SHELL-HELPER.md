# SHELL HELPER

This document includes installation instructions for tools for optimising shell environment.

## Command Completion

```bash
# HELM commands
source <(helm completion zsh)

source <(kubectl completion zsh)
```
