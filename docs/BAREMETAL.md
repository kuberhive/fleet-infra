# BAREMETAL

This document contains topcs related to baremetal configurations

## KERNEL

```bash
sysctl -w fs.inotify.max_user_watches=8388608
sysctl -w fs.inotify.max_user_instances=8388608
sysctl -w fs.inotify.max_queued_events=8388608
sysctl -w fs.inotify.max_user_watches=8388608
sysctl -w fs.inotify.max_user_instances=8388608
sysctl -w fs.inotify.max_queued_events=8388608
```

## STORAGE

### LONGHORN multipathd

See https://longhorn.io/kb/troubleshooting-volume-with-multipath/

The following command(s) resolves the longhorn multipath error

```bash
vi /etc/multipath.conf
# Add the below lines and uncomment to the /etc/multipath.conf
# blacklist {
#     devnode "^sd[a-z0-9]+"
# }

# restart the multipath service
systemctl restart multipathd
```
